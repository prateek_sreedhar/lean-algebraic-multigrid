%=========================================================================
% lamg_example.m
%
% LAMG Example usage: solve a graph Laplacian system with a random
% compatible right-hand side.
%=========================================================================

fprintf('Setting up problem\n');
squareSize = 500;
UL = -1 * ones(squareSize, squareSize);
LR = UL;
UR = zeros(squareSize, squareSize);
LL = UR;
A = [UL UR; LL LR];
for i = 1:(squareSize*2)
    A(i,i) = squareSize - 1;
end
A(squareSize*3/2,squareSize/2) = -1;
A(squareSize/2, squareSize*3/2) = -1;
A(squareSize*3/2,squareSize*3/2) = squareSize;
A(squareSize/2,squareSize/2) = squareSize;         % Zero row-sum (Neumann B.C.)
A = sparse(A);
b = rand(size(A,1), 1);
b = b - mean(b);                % Make RHS compatible with A's null space
inputType = 'laplacian';        % The input matrix A is a graph Laplacian
solver = 'lamg';                % Or 'cmg', or 'direct'

%---------------------------------------------------------------------
% Setup phase: construct a LAMG multi-level hierarchy
%---------------------------------------------------------------------
fprintf('Setting up solver %s\n', solver);
lamg    = Solvers.newSolver(solver, 'randomSeed', 1);
tStart  = tic;
setup   = lamg.setup(inputType, A);
tSetup  = toc(tStart);

%---------------------------------------------------------------------
% Solve phase: set up a random compatible RHS b and solve A*x=b. You can
% repeat this phase for multiple b's without rerunning the setup phase.
%---------------------------------------------------------------------
setRandomSeed(now);
% Turn on debugging printouts during the run
core.logging.Logger.setLevel('lin.api.AcfComputer', core.logging.LogLevel.DEBUG);
fprintf('Solving A*x=b\n');
tStart = tic;
[x, ~, ~, details] = lamg.solve(setup, b, 'errorReductionTol', 1e-8);
tSolve = toc(tStart);
% Turn printouts off
core.logging.Logger.setLevel('lin.api.AcfComputer', core.logging.LogLevel.INFO);
fprintf('------------------------------------------------------------------------\n');

%---------------------------------------------------------------------
% Display statistics
%---------------------------------------------------------------------
disp(setup);
tMvm    = mvmTime(A, 5);
nnz     = numel(nonzeros(A));

fprintf('\n');
fprintf('MVM time [sec]       elapsed %.3f, per nonzero %.2e\n', ...
    tMvm, tMvm / nnz);
fprintf('Setup time [sec]     elapsed %.3f, per nonzero %.2e, in MVM %.2f\n', ...
    tSetup, tSetup / nnz, tSetup / tMvm);
fprintf('Solve time [sec]     elapsed %.3f, per nonzero %.2e, in MVM %.2f\n', ...
    tSolve, normalizedSolveTime(tSolve, details.errorNormHistory) / nnz, tSolve / tMvm);
fprintf('|A*x-b|/|b|          %.2e\n', norm(A*x-b)/norm(b));
if (isfield(details, 'acf'))
    fprintf('Convergence factor   %.3f\n', details.acf);
end