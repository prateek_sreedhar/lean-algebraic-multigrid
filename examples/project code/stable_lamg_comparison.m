%=========================================================================
% Modified LAMG Solver
%=========================================================================
clear all; clc;

% reseed RNG
reset(RandStream.getGlobalStream,sum(100*clock))

% load UF graphs
batchReader = graph.reader.BatchReader();
for i = 1:2757
    batchReader.add('formatType', graph.api.GraphFormat.UF, 'id', i);
end
num_graphs = batchReader.size;

% excel sheet info
data_file = ['data_' datestr(now, 'mm-dd-yy__HH-MM-SS') '.xlsx'];
data = {'Name', 'ID', 'Description', 'Nodes', 'Edges', ...
    'Capacity', 'Delta', 'Number of Levels', 'Setup Time', 'Solve Time' ...
    '|Ax-b|/|b|', 'Convergence Rate', 'Stable Matching Times'};
currRow = 2;

global FILTERING_DELTA
global STABLE_MATCHING_CAPACITY
global STABLE_MATCHING_TIMES
 
capacities = [1 2 5 10 30 90 270 810];
deltas = [0.1 0.05 0.05 0.005 0];

numEdges = batchReader.getNumEdges();

for i = 1:num_graphs
    % only small graphs for now
    if numEdges(i) > 15000000
        continue
    end
    % skip if matrix is not square
    try
        g = batchReader.read(i);
    catch
        continue
    end
    
    % make adjacency matrix binary and symmetric, then calculate laplacian
    B = (g.adjacency ~= 0);
    B = B + B';
    B = (B ~= 0);
    for i = 1:length(B)
        B(i,i) = 0;
    end
    % quit if we've got the zero matrix
    if nnz(B) == 0
        continue
    end
    I = get_largest_cc(B);
    B = B(I,I);
    D = diag(sum(B));
    A = D - B;
    
    % print metadata
    fprintf('Name: %s\n', g.metadata.name); 
    fprintf('ID: %d\n', g.metadata.id);
    
    % update excel cell array with metadata
    data(currRow,:) = { g.metadata.name, g.metadata.id, ...
        g.metadata.description, g.numNodes, g.numEdges, ...
        '', '', '', '', '', '','',''};
    
    % get pair in same connected component
    pair = get_random_connected_pair(A);
    b = zeros(length(A), 1);
    b([pair(1), pair(2)]) = [1, -1];
        
    for cap = capacities
        for delta = deltas
            FILTERING_DELTA = delta;
            STABLE_MATCHING_CAPACITY = cap;
            STABLE_MATCHING_TIMES = {};
            
            % solve Ax = b with and without stable matching
            [nl, sut, svt, err, acf] = custom_solve(A, b, true);
            [nl2, sut2, svt2, err2, acf2] = custom_solve(A, b, false);

            % update excel data
            data{currRow, 6} = cap;
            data{currRow, 7} = delta;
            data([currRow, currRow+1], 8:12) = {
                nl,  sut,  svt,  err,  acf;
                nl2, sut2, svt2, err2, acf2
            };
            timeStr = '';
            for i = 1:length(STABLE_MATCHING_TIMES)
                level = STABLE_MATCHING_TIMES{i}(1);
                time = STABLE_MATCHING_TIMES{i}(2);
                timeStr = [timeStr ', Level ' num2str(level) ': ' num2str(time)];
            end
            data{currRow, 13} = timeStr(2:end);
            
            currRow = currRow + 2;

            fprintf('CAPACITY: %d, DELTA: %0.2e\n', cap, delta);        
            fprintf('Number of levels: %d         ON, %d  OFF\n', nl, nl2);
            fprintf('Setup time      : %.2e  ON, %.2e  OFF\n', sut, sut2);
            fprintf('Solve time      : %.2e  ON, %.2e  OFF\n', svt, svt2);
            fprintf('|Ax-b|/|b|      : %.2e  ON, %.2e  OFF\n', err, err2);
            fprintf('Convergence rate: %.2e  ON, %.2e  OFF\n', acf, acf2);
            fprintf('-------------------------------\n');
        end
    end
end

% write to excel file
xlswrite(data_file, data);