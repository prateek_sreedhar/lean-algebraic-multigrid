function I = get_largest_cc( A )
%GET_LARGEST_CC Summary of this function goes here
%   Detailed explanation goes here
    [numComponents, C] = graphconncomp(A);
    count=hist(C,unique(C));
    [~, i] = max(count);
    I = find(C == i);
end

