function nodes = get_random_connected_pair(A)
    % get two random nodes in connected component
    N = length(A);
    A = A';
    A = A + speye(N);
    curr = zeros(N, 1);
    curr(randi(N)) = 1; % start BFS at random node
    next = A * curr;
    
    while any((curr == 0) ~= (next == 0)) % terminate when no new nodes found
        curr = next;
        next = A * curr;
    end
    
    connected = find(curr ~= 0);
    if length(connected) < 2 % restart if connected component is too small
        nodes = get_random_connected_pair(graph);
        return;
    end
    
    % select two random nodes from connected component
    i = randi(length(connected));
    nodes(1) = connected(i);
    connected(i) = [];
    nodes(2) = connected(randi(length(connected)));
end

