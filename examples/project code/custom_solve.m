function [ num_levels, setup_time, solve_time, err, acf ] = custom_solve(A, b, stable_matching )
    global USE_STABLE_MATCHING
    USE_STABLE_MATCHING = stable_matching;
    
    inputType = 'laplacian';        % The input matrix A is a graph Laplacian
    solver = 'lamg';                % Or 'cmg', or 'direct'
        
    %---------------------------------------------------------------------
    % Setup phase: construct a LAMG multi-level hierarchy
    %---------------------------------------------------------------------
    lamg    = Solvers.newSolver(solver, 'randomSeed', 1);
    tStart  = tic;
    setup   = lamg.setup(inputType, A);
    setup_time = toc(tStart);
    num_levels = setup.numLevels;

    %---------------------------------------------------------------------
    % Solve phase: set up a random compatible RHS b and solve A*x=b. You can
    % repeat this phase for multiple b's without rerunning the setup phase.
    %---------------------------------------------------------------------
    tStart = tic;
    [x, ~, ~, details] = lamg.solve(setup, b, 'errorReductionTol', 1e-8);
    solve_time = toc(tStart);
    acf = details.acf;
    err = norm(A*x-b)/norm(b);
end
