% studentprefs = {
%     [3 1 2],...
%     [2 1 3],...
%     [1 3 2],...
%     [1 2 3]
% };
% programprefs = {
%     [1 2 3 4],...
%     [1 2 3 4],...
%     [3 1 2 4]
% };
% programSlots = [2 1 1];

function studentMatches = stableMatch(studentPrefs, programPrefs, programCaps)
    numStudents = length(studentPrefs);
    numPrograms = length(programPrefs);
    
    studentMatches = zeros(1, numStudents);
    
    offerMade = true;
    while offerMade
        offerMade = false;
        for program = 1:numPrograms
            if isempty(programPrefs{program}); continue; end
            for i = 1:programCaps(program)
                if isempty(programPrefs{program}); break; end

                offerMade = true;
                student = programPrefs{program}(1);
                programPrefs{program}(1) = [];

                oldMatch = studentMatches(student);
                if oldMatch == 0
                    studentMatches(student) = program;
                    programCaps(program) = programCaps(program) - 1;
                else
                    oldRank = find(studentPrefs{student} == oldMatch, 1);
                    newRank = find(studentPrefs{student} == program, 1);
                    if and(~isempty(newRank), newRank < oldRank)
                        studentMatches(student) = program;
                        programCaps(program) = programCaps(program) - 1;
                        programCaps(oldMatch) = programCaps(oldMatch) + 1;
                    end
                end
            end
        end
    end
end