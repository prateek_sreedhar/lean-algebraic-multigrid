function list = preferenceList(affinities)
    [n, ~] = size(affinities);
    list = cell(n, 1);
    affinities_t = affinities';

    for i = 1:n
        [rid, ~, val] = find(affinities_t(:,i));
        [~, I] = sort(val, 'descend');
        list{i} = rid(I);
    end
end