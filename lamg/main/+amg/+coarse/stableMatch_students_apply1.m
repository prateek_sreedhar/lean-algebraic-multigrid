studentPrefs = {
    [3 9 1 6 2 7 8 5 4 10],...
    [8 1 6 10 2 4 3 5 9 7],...
    [1 7 5 10 2 8 6 3 4 9],...
    [10 2 4 8 3 1 6 9 5 7],...
    [10 3 6 9 8 1 7 4 5 2],...
    [1 2 3 4 5 6 7 8 9 10],...
    [4 7 6 10 9 2 5 8 3 1],...
    [5 6 8 3 7 2 1 4 9 10],...
    [9 2 1 6 8 7 4 10 3 5],...
    [1 2 9 7 4  3 5 8 10 6]};
programPrefs = {
    [1 2 3 4 5 6 7 8 9 10],...
    [3 10 2 7 6 4 9 8 5 1],...
	[2 1 8 4 10 9 5 6 7 3],...
	[5 9 10 4 2 1 6 8 3 7],...
	[8 10 4 6 1 3 9 5 2 7],...
	[2 3 9 6 5 10 4 7 1 8],...
	[1 6 8 5 3 7 2 4 9 10],...
	[1 6 9 2 4 7 5 8 3 10],...
	[4 3 1 6 5 7 10 8 9 2],...
	[3 4 10 2 1 6 9 8 7 5]};
programCaps = [1 1 1 2 9 1 2 1 2 1];

numStudents = length(studentPrefs);
numPrograms = length(programPrefs);

studentsFree = 1:numStudents;

programMatches = cell(numPrograms,1);
for program = 1:numPrograms
    programMatches{program} = [];
end
studentMatches = zeros(1, numStudents);

while ~isempty(studentsFree)
    % pop the first student off the list
    student = studentsFree(1);
    studentsFree(1) = [];
    fprintf('student %d is on the market', student);
    
    % check that student has preferences
    if isempty(studentPrefs{student})
        studentsFree(student) = [];
        continue
    end
    
    % pop top program off students preference list
    program = studentPrefs{student}(1);
    studentPrefs{student}(1) = [];
    fprintf('student %d is checking out program %d', student, program);
    
    % have student apply to top program
    if length(programMatches{program}) < programCaps(program)
        % program's free
        programMatches{program}(end+1) = student;
        studentMatches(student) = program;
        fprintf('student %d matched with program %d', student, program);
    else
        % program's full
        for i = 1:length(programMatches{program})
            matchedAlready = programMatches{program}(i);
            if find(programPrefs{program} == matchedAlready, 1) > find(programPrefs{program} == student, 1)
                % program prefers new student
                programMatches{program}(i) = student;
                studentMatches(student) = program;
                % add old student back
                if ~isempty(studentPrefs{matchedAlready})
                    studentsFree(end+1) = matchedAlready;
                else
                    studentsLost(end+1) = matchedAlready;
                end
                return
            else
                % else, program prefers old match
                if ~isempty(studentPrefs{student})
                    studentsFree(end+1) = student;
                else
                    studentsLost(end+1) = student;
                end
            end
        end
    end
end