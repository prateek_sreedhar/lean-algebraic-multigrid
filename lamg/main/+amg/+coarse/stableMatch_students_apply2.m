% studentprefs = {
%     [3 1 2],...
%     [2 1 3],...
%     [1 3 2],...
%     [1 2 3]
% };
% programprefs = {
%     [1 2 3 4],...
%     [1 2 3 4],...
%     [3 1 2 4]
% };
% programSlots = [2 1 1];

function programMatches = stableMatch(studentPrefs, programPrefs, programCaps)
    numStudents = length(studentPrefs);
    numPrograms = length(programPrefs);
    
    studentsFree = 1:numStudents;
    studentsLost = [];
    
    programMatches = cell(numPrograms, 1);
    for i = 1:numPrograms
        programMatches{i} = [];
    end
    
    while ~isempty(studentsFree)
        % pop the first student off the list
        student = studentsFree(1);
        studentsFree(1) = [];
        
        % check that student has preferences
        if isempty(studentPrefs{student})
            studentsLost(end+1) = student;
            studentsFree(student) = [];
            continue
        end
        
        % pop top program off students preference list
        program = studentPrefs{student}(1);
        studentPrefs{student}(1) = [];
        
        % student applies to program
        if length(programMatches{program}) < programCaps(program)
            % program's free
            programMatches{program}(end+1) = student;
            studentsFree(student) = [];
        else
            % program's full
            for i = 1:length(programMatches{program})
                matchedAlready = matched
            end
        end
            
    end
    
    offerMade = true;
    while offerMade
        offerMade = false;
        for program = 1:numPrograms
            if isempty(programPrefs{program}); continue; end
            for i = 1:programCaps(program)
                if isempty(programPrefs{program}); break; end
                
                offerMade = true;
                student = programPrefs{program}(1);
                programPrefs{program}(1) = [];
                
                oldMatch = studentMatches(student);
                if oldMatch == 0
                    studentMatches(student) = program;
                    programCaps(program) = programCaps(program) - 1;
                else
                    oldRank = find(studentPrefs{student} == oldMatch, 1);
                    newRank = find(studentPrefs{student} == program, 1);
                    if and(~isempty(newRank), newRank < oldRank)
                        studentMatches(student) = program;
                        programCaps(program) = programCaps(program) - 1;
                        programCaps(oldMatch) = programCaps(oldMatch) + 1;
                    end
                end
            end
        end
    end
end